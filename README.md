## Form Generator Library

[[_TOC_]] 

## 📝 Overview

The Form Generator library for Vue TypeScript provides functionality for generating forms based on supplied schemas, simplifying the form creation process within Coscine applications. The library includes a [SHACL](https://www.w3.org/TR/shacl/) Form Generator [Vue 2](https://vuejs.org/) component, which is used in both the [Coscine](https://coscine.de/) and [AIMS project](https://www.aims-projekt.de/) to streamline the process of generating forms from schema definitions.

## 📖 Usage

### Build
* `yarn install`
* `yarn build`
### Testing
* `yarn test`
### Linting
* `yarn lint:fix`
### Links
Check the property definitions of `/src/FormGenerator.vue` for understanding the requirements.

```html
<FormGenerator
  :directly-create-rdf-type="directlyCreateRdfType"
  :disabled-mode="formDisabled"
  :fixed-value-mode="fixedValueMode"
  :fixed-values="fixedValues"
  :form-data="metadata"
  :form-data-mime-type="metadataMimeType"
  :locale="locale"
  :selected-shape="applicationProfileUrl"
  :shape-mime-type="shapeMimeType"
  :shapes="applicationProfile"
  :validation-context="validationContext"
  :defined-subject="definedSubject"
  :ignore-template-values="ignoreTemplateValues"
  :template-mode="templateMode"
  :class-receiver="implementedReceiveClass"
  :user-receiver="implementedReceiveUser"
  @update:fixed-values="inputFixedValues"
  @update:form-data="inputMetadata"
  @isValid="isValid"
  @isValidating="isValidating"
/>
```

## Usage

How to include in your Vue project:

```js
import FormGenerator from "@coscine/form-generator";
import "@coscine/form-generator/dist/style.css";
```

## 👥 Contributing

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://git.rwth-aachen.de/coscine/frontend/libraries/form-generator)

(For a specific branch, build such a URL: `https://gitpod.io/#https://git.rwth-aachen.de/coscine/frontend/libraries/form-generator/tree/{branch}`)

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
