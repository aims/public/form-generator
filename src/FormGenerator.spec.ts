/* Testing imports */
import { mount } from '@vue/test-utils';

/* Tested Component */
import FormGenerator from '@/FormGenerator.vue';

import {
  dataCite,
  dropShapeAnalysis,
  listApplicationProfile,
  radarAP,
} from '@/data/example/applicationProfile';
import {
  dataCiteApFixed,
  listFixedValues,
  radar,
} from '@/data/example/fixedValues';
import {
  dataCiteMetadata,
  dropShapeAnalysisMetadata,
  listMetadata,
} from '@/data/example/metadata';
import factory from 'rdf-ext';

import type { Dataset, Quad } from '@rdfjs/types';

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe('FormGenerator.vue', () => {
  const defaultDisplayName = 'Tester Testington';

  /* Checks for correct functionality of lists */
  test('listWithoutMetadata', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        fixedValues: listFixedValues,
        selectedShape: 'https://aims-projekt.de/ap/person/',
        shapes: listApplicationProfile,
        shapesMimeType: 'text/turtle',
        userReceiver: async () => {
          return {
            displayName: defaultDisplayName,
          };
        },
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    const isValid = wrapper.emitted('isValid') ?? [[{ conforms: false }]];

    expect(isValid).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(isValid!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(isValid![0][0]).toEqual(
      expect.objectContaining({
        conforms: false,
      }),
    );

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });
    expect(allTextFields.length).toBe(5);

    // Default value has been set
    const defaultUserField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === defaultDisplayName,
    );
    expect(defaultUserField.length).toBe(1);

    // Fixed value has been set
    const fixedUserField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === 'NichtMustermann',
    );
    expect(fixedUserField.length).toBe(1);

    const email = allTextFields.find(
      (component) =>
        component.vm.$props?.nodeName === 'http://xmlns.com/foaf/0.1/mbox',
    );

    if (email) {
      const textInput = email.find('input[type="text"]');
      await textInput.setValue('example@example.com');
    }

    await wrapper.vm.$nextTick();

    // Wait for 1 second until validation has been done
    await sleep(1000);

    expect(
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      wrapper.emitted('isValid')![wrapper.emitted('isValid')!.length - 1][0],
    ).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );
  });

  /* Checks for correct functionality of lists */
  test('listWithMetadata', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        fixedValues: listFixedValues,
        formData: listMetadata,
        formDataMimeType: 'text/turtle',
        selectedShape: 'https://aims-projekt.de/ap/person/',
        shapes: listApplicationProfile,
        shapesMimeType: 'text/turtle',
        userReceiver: async () => {
          return {
            displayName: defaultDisplayName,
          };
        },
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    expect(wrapper.emitted('isValid')).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')![0][0]).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });
    expect(allTextFields.length).toBe(5);

    // Default value has been set
    const defaultUserField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === 'Max',
    );
    expect(defaultUserField.length).toBe(1);

    // Fixed value has been set
    const fixedUserField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === 'Mustermann',
    );
    expect(fixedUserField.length).toBe(1);

    expect(
      wrapper.find('.InputList .multiselect__single').exists(),
    ).toBeTruthy();
    expect(wrapper.get('.InputList .multiselect__single').html()).toContain(
      'Banana',
    );
  });

  /* Checks for correct functionality of metadata display */
  test('complexWithMetadata', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        formData: dropShapeAnalysisMetadata,
        formDataMimeType: 'text/turtle',
        selectedShape: 'https://purl.org/coscine/ap/sfb985/dropShapeAnalysis/',
        shapes: dropShapeAnalysis,
        shapesMimeType: 'text/turtle',
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    expect(wrapper.emitted('isValid')).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')![0][0]).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });

    // Metadata value has been set
    const defaultUserField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === 'Tester',
    );
    expect(defaultUserField.length).toBe(2);
  });

  /* Checks for correct functionality of empty metadatasets */
  test('listWithEmptyMetadataFixedValues', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        fixedValues: {},
        formData: factory.dataset() as unknown as Dataset<Quad, Quad>,
        selectedShape: 'https://aims-projekt.de/ap/agent/',
        shapes: listApplicationProfile,
        shapesMimeType: 'text/turtle',
        userReceiver: async () => {
          return {
            displayName: defaultDisplayName,
          };
        },
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    expect(wrapper.emitted('isValid')).toBeFalsy();

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });
    expect(allTextFields.length).toBe(1);

    await allTextFields[0].get('input').setValue('example@example.org');

    // Wait for validation debounce
    await sleep(1000);

    expect(wrapper.emitted('isValid')).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')![0][0]).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );
  });

  /* Checks for correct functionality of metadata display in inherited APs */
  test('dataCiteAp', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        formData: dataCiteMetadata,
        formDataMimeType: 'text/turtle',
        ignoreTemplateValues: true,
        selectedShape:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
        shapes: dataCite,
        shapesMimeType: 'text/turtle',
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000);

    expect(wrapper.emitted('isValid')).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')![0][0]).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });

    // Metadata value has been set
    const creatorField = allTextFields.filter(
      (entry) => entry.vm?.object?.value === 'Cool name',
    );
    expect(creatorField.length).toBe(1);
  });

  /* Checks for correct functionality of metadata display in inherited APs with fixedValues */
  test('dataCiteApFixed', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        fixedValues: dataCiteApFixed,
        selectedShape:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
        shapes: dataCite,
        shapesMimeType: 'text/turtle',
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 2 seconds until everything is set up
    await sleep(2000);

    expect(wrapper.emitted('isValid')).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')!.length).toBe(1);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    expect(wrapper.emitted('isValid')![0][0]).toEqual(
      expect.objectContaining({
        conforms: true,
      }),
    );

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });

    // Metadata value has been set
    const creatorField = allTextFields.filter(
      (entry) => entry.vm?.object?.value === 'TestName',
    );
    expect(creatorField.length).toBe(1);

    // Metadata value has been set
    const agentIdentifierField = allTextFields.filter(
      (entry) => entry.vm?.object?.value === 'TestAIdentifier',
    );
    expect(agentIdentifierField.length).toBe(1);

    // No Quantum Entanglement
    const lockedInput = allTextFields.filter(
      (entry) => entry.vm?.object?.value === 'TestAFill',
    );
    expect(lockedInput.length).toBe(1);
    expect(lockedInput?.at(0)?.vm.$props.locked).toBeTruthy();

    const unlockedInput = allTextFields.filter(
      (entry) => entry.vm?.object?.value === 'TestPOCA',
    );
    expect(unlockedInput.length).toBe(1);
    expect(unlockedInput?.at(0)?.vm.$props.locked).toBeFalsy();

    expect(wrapper.find('.invisible').exists()).toBeFalsy();
  });

  /*Checks for correct functionality of removing the fixed values */
  test('dataCiteApFixed', async () => {
    const wrapper = mount(FormGenerator, {
      propsData: {
        fixedValues: radar,
        selectedShape: 'https://purl.org/coscine/ap/radar/',
        shapes: radarAP,
        shapesMimeType: 'text/turtle',
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 2 seconds until everything is set up
    await sleep(2000);

    const allTextFields = wrapper.findAllComponents({ name: 'InputTextField' });

    // Default value has been set
    const defaultTitleField = allTextFields.filter(
      (entry) => entry.vm?.$data?.object?.value === 'TestTitle',
    );

    expect(defaultTitleField.length).toBe(1);

    // Remove the value and set to empty field
    if (defaultTitleField.length > 0) {
      const titleFieldVm = defaultTitleField[0].vm;
      titleFieldVm.$data.object.value = ''; // clear the value
      await wrapper.vm.$nextTick();
    }

    // Check if fixedValues is defined and not null
    const fixedValues = wrapper.vm.$props.fixedValues;
    expect(fixedValues).toBeDefined();

    // Check if the specific property exists before accessing it
    if (fixedValues) {
      expect(
        fixedValues['http://purl.org/dc/terms/title'][
          'https://purl.org/coscine/defaultValue'
        ]?.length === 0,
      );
    }
  });
});
