import {
  FixedFixedValue,
  InvisibleFixedValue,
  type FixedValues,
  type ValueType,
  type FixedValueObject,
  DefaultFixedValue,
} from '@/types/fixedValues';
import type { Dataset, Quad_Object, Quad_Subject } from '@rdfjs/types';
import type { LooseRequired } from '@vue/shared';
import factory from 'rdf-ext';
import prefixes from '@zazuko/prefixes';

export const useFixedValues = (
  props: Readonly<
    Readonly<
      LooseRequired<{
        disabledMode: boolean;
        fixedValues: FixedValues;
        fixedValueMode: boolean;
        property: Quad_Subject;
        targetClass: Quad_Object;
      }>
    >
  >,
  dataset: Ref<Dataset>,
  emit: (event: string, ...args: unknown[]) => void,
  nodeName: Ref<string>,
  required: Ref<boolean>,
  values: Ref<Quad_Object[]>,
) => {
  const { disabledMode, fixedValues, fixedValueMode, property, targetClass } =
    toRefs(props);

  const invisible = ref(false);
  const locked = ref(false);

  const fixedValueDisabled = computed(() => {
    return values.value.map(
      (entry) =>
        disabledMode.value || !entry || (required.value && invisible.value),
    );
  });

  const fieldCanBeInvisible = computed(() => {
    return (
      (required.value &&
        values.value.some((entry) => entry.value !== '') &&
        locked.value) ||
      !required.value
    );
  });

  const getClonedFixedValueObject = (nodeName: string): FixedValueObject => {
    const entity = fixedValues.value[nodeName];
    if (entity) {
      return {
        [DefaultFixedValue]: entity[DefaultFixedValue]
          ? [...entity[DefaultFixedValue]]
          : [],
        [FixedFixedValue]: entity[FixedFixedValue]
          ? [...entity[FixedFixedValue]]
          : [],
        [InvisibleFixedValue]: entity[InvisibleFixedValue]
          ? [...entity[InvisibleFixedValue]]
          : [],
      };
    }
    return {};
  };

  const removeIndividualFixedValue = (
    currentFixedValueObject: FixedValueObject,
    identifier: keyof FixedValueObject,
    givenValue: ValueType[],
  ) => {
    if (currentFixedValueObject[identifier]) {
      currentFixedValueObject[identifier] = currentFixedValueObject[
        identifier
      ]?.filter(
        (entry) =>
          entry.targetClass &&
          !givenValue.some(
            (otherEntry) => entry.targetClass === otherEntry.targetClass,
          ),
      );
    }
  };

  const setIndividualFixedValue = (
    currentFixedValueObject: FixedValueObject,
    identifier: keyof FixedValueObject,
    givenValue: ValueType[],
  ) => {
    if (currentFixedValueObject[identifier]) {
      removeIndividualFixedValue(
        currentFixedValueObject,
        identifier,
        givenValue,
      );
      currentFixedValueObject[identifier]?.push(...givenValue);
    } else {
      currentFixedValueObject[identifier] = givenValue;
    }
  };

  const extractForFixedValues = (values: Quad_Object[]) => {
    const extractedValues: ValueType[] = [];
    for (const entry of values.filter((entry) => entry.value !== '')) {
      let termType: ValueType['type'] = 'uri';
      if (entry.termType === 'Literal') {
        termType = 'literal';
      } else if (entry.termType === 'BlankNode') {
        termType = 'bnode';
      }
      const valueObject: ValueType = {
        value: entry.value,
        type: termType,
        datatype:
          entry.termType === 'Literal' ? entry.datatype.value : undefined,
        targetClass: targetClass.value.value,
      };
      extractedValues.push(valueObject);
    }
    return extractedValues;
  };

  const inputFixedValues = (object: FixedValueObject, nodeName: string) => {
    emit('update:fixed-values', nodeName, object);
  };

  const setFixedValuesSpecific = (
    defaultFixedValue: ValueType[],
    fixedFixedValue: ValueType[],
    invisible: ValueType[],
    nodeName: string,
  ) => {
    const currentFixedValueObject = getClonedFixedValueObject(nodeName);

    setIndividualFixedValue(
      currentFixedValueObject,
      InvisibleFixedValue,
      invisible ? invisible : [],
    );
    setIndividualFixedValue(
      currentFixedValueObject,
      FixedFixedValue,
      fixedFixedValue,
    );
    removeIndividualFixedValue(
      currentFixedValueObject,
      DefaultFixedValue,
      fixedFixedValue,
    );

    if (
      defaultFixedValue.length === 0 &&
      fixedFixedValue.length === 0 &&
      values.value.every((entry) => entry.value === '')
    ) {
      currentFixedValueObject[DefaultFixedValue] = [];
    } else {
      setIndividualFixedValue(
        currentFixedValueObject,
        DefaultFixedValue,
        defaultFixedValue,
      );
    }

    removeIndividualFixedValue(
      currentFixedValueObject,
      FixedFixedValue,
      defaultFixedValue,
    );

    inputFixedValues(currentFixedValueObject, nodeName);
  };

  const setFixedValues = (values: Quad_Object[]) => {
    const extractedFixedValue = extractForFixedValues(values);
    const invisibleType: ValueType = {
      value: invisible.value ? '1' : '0',
      type: 'literal',
      targetClass: targetClass.value.value,
    };
    setFixedValuesSpecific(
      locked.value ? [] : extractedFixedValue,
      locked.value ? extractedFixedValue : [],
      [invisibleType],
      nodeName.value,
    );
  };

  const updateFixedValues = (values: Quad_Object[]) => {
    if (fixedValueMode.value) {
      if (
        fixedValues.value[nodeName.value] &&
        fixedValues.value[nodeName.value][FixedFixedValue] &&
        fixedValues.value[nodeName.value][FixedFixedValue]?.some(
          (val) =>
            !val.targetClass || val.targetClass === targetClass.value.value,
        )
      ) {
        locked.value = true;
      } else {
        locked.value = false;
      }
      setFixedValues(values);
    }
  };

  const updateMetadataValue = (
    values: Quad_Object[],
    fixedValueTransfer = true,
  ) => {
    emit('update:metadata', nodeName.value, values);
    if (fixedValueTransfer) {
      updateFixedValues(values);
    }
  };

  const valueTypesToQuadObjects = (valueTypes: ValueType[]): Quad_Object[] => {
    const mappedType = valueTypes.map((fixedValue) => {
      if (
        fixedValue.targetClass &&
        fixedValue.targetClass !== targetClass.value.value
      ) {
        return null;
      }
      if (fixedValue.type === 'literal') {
        return factory.literal(
          fixedValue.value,
          fixedValue.datatype
            ? factory.namedNode(fixedValue.datatype)
            : undefined,
        );
      } else if (fixedValue.type === 'bnode') {
        return factory.blankNode();
      } else {
        return factory.namedNode(fixedValue.value);
      }
    });
    return mappedType.filter((result) => result !== null) as Quad_Object[];
  };

  const initDefaultValues = () => {
    if (
      (!values.value.length ||
        values.value.every((entry) => entry.value === '')) &&
      !locked.value
    ) {
      const fixedValueObject = fixedValues.value[nodeName.value];
      const datasetMatches = dataset.value.match(
        property.value,
        factory.namedNode(prefixes.sh + 'defaultValue'),
      );
      if (
        fixedValueObject &&
        fixedValueObject[DefaultFixedValue]?.some(
          (entry) =>
            (!entry.targetClass ||
              entry.targetClass === targetClass.value.value) &&
            entry.value !== '',
        )
      ) {
        updateMetadataValue(
          valueTypesToQuadObjects(fixedValueObject[DefaultFixedValue]),
        );
      } else if (datasetMatches.size) {
        updateMetadataValue(
          Array.from(datasetMatches).map((quad) => quad.object),
        );
      }
    }
  };

  const initFixedValues = () => {
    const fixedValueObject = fixedValues.value[nodeName.value];
    if (fixedValueObject) {
      const fixedValueList = fixedValueObject[FixedFixedValue];
      if (
        fixedValueList?.some(
          (entry) =>
            (!entry.targetClass ||
              entry.targetClass === targetClass.value.value) &&
            entry.value !== '',
        )
      ) {
        if (
          !values.value.length ||
          values.value.every((entry) => entry.value === '')
        ) {
          updateMetadataValue(valueTypesToQuadObjects(fixedValueList));
        }
        locked.value = true;
      }
      const invisibleList = fixedValueObject[InvisibleFixedValue];
      invisible.value =
        invisibleList?.length !== 0 &&
        invisibleList?.every((entry) => entry.value === '1') === true;
    }
  };

  const inputFullFixedValues = (
    fixedValues: FixedValues,
    targetClass: Quad_Object,
  ) => {
    for (const nodeName of Object.keys(fixedValues)) {
      const valueObject = fixedValues[nodeName];
      const invisible = (valueObject[InvisibleFixedValue] ?? []).filter(
        (entry) => entry.targetClass === targetClass.value,
      );
      const defaultValue = (valueObject[DefaultFixedValue] ?? []).filter(
        (entry) => entry.targetClass === targetClass.value,
      );
      const fixedValue = (valueObject[FixedFixedValue] ?? []).filter(
        (entry) => entry.targetClass === targetClass.value,
      );

      setFixedValuesSpecific(defaultValue, fixedValue, invisible, nodeName);
    }
  };

  const changeLockable = () => {
    locked.value = !locked.value;
    setFixedValues(values.value);
  };

  const changeVisibility = () => {
    if (!invisible.value && fieldCanBeInvisible.value) {
      invisible.value = true;
    } else {
      invisible.value = false;
    }
    setFixedValues(values.value);
  };

  watch(fixedValues, () => {
    initFixedValues();
  });

  return {
    changeLockable,
    changeVisibility,
    initDefaultValues,
    initFixedValues,
    inputFullFixedValues,
    updateMetadataValue,
    fieldCanBeInvisible,
    fixedValueDisabled,
    invisible,
    locked,
  };
};
