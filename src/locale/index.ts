export default {
  en: {
    selectPlaceholder: 'Select...',
    invisibilityInfo:
      "Hides this field from view during file uploads and metadata editing. Required fields can't be hidden. Visibility can be adjusted later",
    lockInfo:
      'Locks the default value to prevent changes. Can be unlocked later.',
    plusInfo:
      'Adds a field of the same type. Duplicate values are not supported. Remove added fields via the minus button. Can be adjusted later.',
  },
  de: {
    selectPlaceholder: 'Auswählen...',
    invisibilityInfo:
      'Verbirgt dieses Feld beim Hochladen von Dateien und Bearbeiten von Metadaten. Pflichtfelder können nicht verborgen werden. Sichtbarkeit kann später angepasst werden.',
    lockInfo:
      'Sperrt den Standardwert, um Änderungen zu verhindern. Kann später entsperrt werden.',
    plusInfo:
      'Fügt ein Feld desselben Typs hinzu. Doppelte Werte werden nicht unterstützt. Hinzugefügte Felder können über den Minus-Button entfernt werden. Kann später angepasst werden.',
  },
};
