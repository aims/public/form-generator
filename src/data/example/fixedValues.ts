import {
  DefaultFixedValue,
  FixedFixedValue,
  FixedValues,
  InvisibleFixedValue,
} from '@/types/fixedValues';

export const listFixedValues = {
  'http://example.org/fruits': {
    [DefaultFixedValue]: [
      {
        value: 'Orange',
        type: 'literal',
      },
    ],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
      },
    ],
  },
  'http://xmlns.com/foaf/0.1/firstName': {
    [DefaultFixedValue]: [
      {
        value: '{ME}',
        type: 'literal',
      },
    ],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
      },
    ],
  },
  'http://xmlns.com/foaf/0.1/lastName': {
    [FixedFixedValue]: [
      {
        value: 'NichtMustermann',
        type: 'literal',
      },
    ],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
      },
    ],
  },
} satisfies FixedValues;

export const dataCiteApFixed = {
  'http://purl.org/dc/terms/title': {
    [DefaultFixedValue]: [
      {
        value: 'TestTitle',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
  },
  'http://purl.org/dc/terms/creator': {
    [DefaultFixedValue]: [
      {
        value: 'b8',
        type: 'bnode',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
      {
        value: 'TestName',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
  },
  'http://schema.org/affiliation': {
    [DefaultFixedValue]: [
      {
        value: 'TestPOCA',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
    [FixedFixedValue]: [
      {
        value: 'TestAFill',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
  },
  'http://purl.org/spar/datacite/AgentIdentifierScheme': {
    [DefaultFixedValue]: [
      {
        value: 'DAI',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
  },
  'http://purl.org/spar/datacite/AgentIdentifier': {
    [DefaultFixedValue]: [
      {
        value: 'TestAIdentifier',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/author/',
      },
    ],
  },
  'http://schema.org/contactPoint': {
    [DefaultFixedValue]: [
      {
        value: 'b9',
        type: 'bnode',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
  },
  'http://xmlns.com/foaf/0.1/name': {
    [DefaultFixedValue]: [
      {
        value: 'TestPOCN',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
  },
  'http://schema.org/email': {
    [DefaultFixedValue]: [
      {
        value: 'TestPOCE',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/pointOfContact/',
      },
    ],
  },
  'http://schema.org/description': {
    [DefaultFixedValue]: [
      {
        value: 'TestDescri',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
  },
  'http://purl.org/dc/terms/subject': {
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
    [DefaultFixedValue]: [
      {
        value: 'Computer and Information Science',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass:
          'https://purl.org/coscine/ap/Dataverse_Citation_Metadata/minimal/',
      },
    ],
  },
} satisfies FixedValues;

export const radar = {
  'http://purl.org/dc/terms/creator': {
    [DefaultFixedValue]: [
      {
        value: '{ME}',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
  },
  'http://purl.org/dc/terms/created': {
    [DefaultFixedValue]: [
      {
        value: '2024-07-30',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#date',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
  },
  'http://purl.org/dc/terms/title': {
    [DefaultFixedValue]: [
      {
        value: 'TestTitle',
        type: 'literal',
        datatype: 'http://www.w3.org/2001/XMLSchema#string',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
  },
  'http://purl.org/dc/terms/rights': {
    [DefaultFixedValue]: [],
    [FixedFixedValue]: [],
    [InvisibleFixedValue]: [
      {
        value: '0',
        type: 'literal',
        targetClass: 'https://purl.org/coscine/ap/radar/',
      },
    ],
  },
} satisfies FixedValues;
