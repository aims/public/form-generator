// From @coscine/api-client

import type { Quad_Object } from '@rdfjs/types';

/**
 * Bilingual labels
 * @export
 * @interface BilingualLabels
 */
export interface BilingualLabels {
  enPagination?: Pagination;
  /**
   * English labels
   * @type {Array<Label>}
   * @memberof BilingualLabels
   */
  en?: Array<Label> | null;
  dePagination?: Pagination;
  /**
   * German labels
   * @type {Array<Label>}
   * @memberof BilingualLabels
   */
  de?: Array<Label> | null;
}

/**
 * Bilingual labels
 * @export
 * @interface BilingualLabel
 */
export interface BilingualLabel {
  /**
   * English labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  en?: Label | null;
  /**
   * German labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  de?: Label | null;
}

/**
 * Label of a vocabulary entry
 * @export
 * @interface Label
 */
export interface Label {
  name?: string | null;
  value?: string | null;
}

export interface ObjectLabel {
  name?: string | null;
  value?: Quad_Object | null;
}

/**
 * Represents pagination information for a collection of items.
 * @export
 * @interface Pagination
 */
export interface Pagination {
  /**
   * Gets or sets the current page number.
   * @type {number}
   * @memberof Pagination
   */
  currentPage?: number;
  /**
   * Gets or sets the total number of pages.
   * @type {number}
   * @memberof Pagination
   */
  totalPages?: number;
  /**
   * Gets or sets the number of items per page.
   * @type {number}
   * @memberof Pagination
   */
  pageSize?: number;
  /**
   * Gets or sets the total count of items across all pages.
   * @type {number}
   * @memberof Pagination
   */
  totalCount?: number;
  /**
   * Gets a value indicating whether there is a previous page.
   * @type {boolean}
   * @memberof Pagination
   */
  hasPrevious?: boolean;
  /**
   * Gets a value indicating whether there is a next page.
   * @type {boolean}
   * @memberof Pagination
   */
  hasNext?: boolean;
}
