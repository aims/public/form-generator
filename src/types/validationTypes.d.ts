import type ValidationReport from 'rdf-validate-shacl/src/validation-report';
import type {
  DataFactory,
  DatasetCore,
  DatasetCoreFactory,
  Quad,
} from '@rdfjs/types';

export type ValidationType = Array<
  ValidationReport.ValidationResult<
    DataFactory<Quad, Quad> &
      DatasetCoreFactory<Quad, Quad, DatasetCore<Quad, Quad>>
  >
>;
