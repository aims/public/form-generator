<template>
  <div id="FormGenerator">
    <div v-for="(pathToProperty, index) in pathList" :key="index">
      <WrapperInput
        :wrapper-dataset="dataset"
        :property="pathToProperty.subject"
        :metadata="metadata"
        :metadata-subject="metadataSubject"
        :fixed-value-mode="fixedValueMode"
        :fixed-values="internalFixedValues"
        :disabled-mode="disabledMode"
        :language-locale="locale"
        :target-class="targetClass"
        :error-messages="errorMessages"
        :template-mode="templateMode"
        :class-receiver="classReceiver"
        :instance-receiver="instanceReceiver"
        :user-receiver="userReceiver"
        @triggerUpdateDataset="triggerUpdateDataset"
        @update:fixed-values="inputFixedValues"
        @update:metadata="updateMetadata"
        @triggerValidation="
          triggerMetadataValidation(
            cleanedMetadata,
            dataset,
            metadataSubject,
            validationContext,
            ignoreTemplateValues,
          )
        "
      />
    </div>
  </div>
</template>

<script lang="ts">
import i18n from '@/plugins/i18n';

import type { Dataset, Quad, Quad_Object, Quad_Subject } from '@rdfjs/types';
import factory from 'rdf-ext';

import LinkedDataHandler from '@/base/LinkedDataHandler';
import prefixes from '@zazuko/prefixes/prefixes';

import {
  DefaultFixedValue,
  FixedFixedValue,
  InvisibleFixedValue,
  type FixedValueObject,
  type FixedValues,
  type ValueType,
} from '@/types/fixedValues';
import type DatasetExt from 'rdf-ext/lib/Dataset';
import {
  getQuads,
  retrieveChildren,
  retrieveInstance,
} from '@/util/linkedData';
import type { User } from '@/types/user';
import { computedAsync } from '@vueuse/core';

export default defineComponent({
  name: 'FormGenerator',
  extends: LinkedDataHandler,
  props: {
    definedSubject: {
      default: () => factory.blankNode(),
      type: Object as PropType<Quad_Subject>,
    },
    directlyCreateRdfType: {
      default: true,
      type: Boolean,
    },
    disabledMode: {
      default: false,
      type: Boolean,
    },
    fixedValueMode: {
      default: false,
      type: Boolean,
    },
    fixedValues: {
      default: () => ({}),
      type: Object as PropType<FixedValues>,
    },
    formData: {
      default: () => factory.dataset() as unknown as Dataset,
      type: [String, Object] as PropType<string | Dataset>,
    },
    formDataMimeType: {
      default: 'application/ld+json',
      type: String,
    },
    ignoreTemplateValues: {
      default: false,
      type: Boolean,
    },
    locale: {
      default: 'en',
      type: String,
    },
    selectedShape: {
      default: 'https://purl.org/coscine/ap/',
      type: [String, Object] as PropType<string | Quad_Subject>,
    },
    shapes: {
      default: '',
      type: [String, Object, null] as PropType<string | Dataset | null>,
    },
    shapesMimeType: {
      default: 'application/ld+json',
      type: String,
    },
    templateMode: {
      default: false,
      type: Boolean,
    },
    validationContext: {
      default: '',
      type: String,
    },
    classReceiver: {
      default: () => retrieveChildren,
      type: Function as PropType<typeof retrieveChildren>,
    },
    instanceReceiver: {
      default: () => retrieveInstance,
      type: Function as PropType<typeof retrieveInstance>,
    },
    userReceiver: {
      default: () => {
        return async () => ({});
      },
      type: Function as PropType<() => Promise<User>>,
    },
  },
  setup(props) {
    const { shapes, shapesMimeType, selectedShape } = toRefs(props);

    const retrieveDataset = async (
      shapes: string,
      mimeType: string,
      selectedShape: string,
    ): Promise<Dataset> => {
      if (shapes !== '') {
        return await getQuads(
          shapes,
          mimeType,
          selectedShape ? selectedShape : 'https://purl.org/coscine/ap/',
        );
      }
      return factory.dataset() as unknown as Dataset;
    };

    const selectedShapeValue = computed(() => {
      if (typeof selectedShape.value === 'string') {
        return selectedShape.value;
      }
      return selectedShape.value.value;
    });

    const dataset = computedAsync(
      async () => {
        if (typeof shapes.value === 'string' && shapes.value !== '') {
          return await retrieveDataset(
            shapes.value,
            shapesMimeType.value,
            selectedShapeValue.value,
          );
        } else if (shapes.value !== null && shapes.value !== '') {
          return factory.dataset(
            Array.from(shapes.value),
          ) as unknown as Dataset;
        }
        return factory.dataset() as unknown as Dataset;
      },
      factory.dataset() as unknown as Dataset, // initial state
    );

    return {
      dataset,
      localeKey: i18n.global.locale,
      retrieveDataset,
      selectedShapeValue,
    };
  },
  data() {
    return {
      cleanedMetadata: factory.dataset() as unknown as Dataset,
      internalFixedValues: {} as FixedValues,
      metadata: factory.dataset() as unknown as Dataset,
    };
  },
  computed: {
    metadataSubject(): Quad_Subject {
      let subjectNode = this.definedSubject;
      const rdfType = factory.namedNode(prefixes.rdf + 'type');
      if (this.metadata.size) {
        for (const entry of this.metadata.match(undefined, rdfType)) {
          subjectNode = entry.subject;
          break;
        }
      }
      return subjectNode;
    },
    orders(): Record<string, number> {
      return Array.from(
        this.dataset.match(null, factory.namedNode(prefixes.sh + 'order')),
      ).reduce(
        (result, item) => {
          result[item.subject.value] = parseInt(item.object.value);
          if (isNaN(result[item.subject.value]))
            result[item.subject.value] = Number.MAX_VALUE;
          return result;
        },
        {} as Record<string, number>,
      );
    },
    pathList(): Quad[] {
      const rootShapes: Quad_Object[] = [this.selectedShapeNode];
      const toScan: Quad_Object[] = [this.selectedShapeNode];
      while (toScan.length > 0) {
        const scanningShape = toScan.pop();
        if (scanningShape) {
          const importedShapes = Array.from(
            this.dataset.match(
              scanningShape,
              factory.namedNode(prefixes.sh + 'node'),
            ),
          ).map((tempQuad) => tempQuad.object);
          for (const importedShape of importedShapes) {
            if (
              !rootShapes.some(
                (rootShape) => rootShape.value === importedShape.value,
              )
            ) {
              rootShapes.push(importedShape);
              toScan.push(importedShape);
            }
          }
        }
      }
      const properties = Array.from(
        this.dataset.filter(
          (quad) =>
            quad.object.value === prefixes.sh + 'NodeShape' &&
            rootShapes.some(
              (rootShape) => rootShape.value === quad.subject.value,
            ),
        ),
      ).map((quad) => {
        const propertyObjects = Array.from(
          this.dataset.filter(
            (filterQuad) =>
              filterQuad.subject.value === quad.subject.value &&
              filterQuad.predicate.value === prefixes.sh + 'property',
          ),
        ).map((tempQuad) => tempQuad.object.value);

        const properties = Array.from(
          this.dataset.filter(
            (quad) =>
              quad.predicate.value === prefixes.sh + 'path' &&
              propertyObjects.includes(quad.subject.value),
          ),
        );

        return properties;
      });

      return properties
        .flat(1)
        .filter((entry) => entry.object.value !== prefixes.rdf + 'type')
        .sort((entry1, entry2) => this.compareEntries(entry1, entry2));
    },
    selectedShapeNode(): Quad_Subject {
      if (typeof this.selectedShape === 'string') {
        return factory.namedNode(
          this.selectedShape
            ? this.selectedShape
            : 'https://purl.org/coscine/ap/',
        );
      }
      return this.selectedShape;
    },
    targetClass(): Quad_Object {
      for (const targetClass of this.dataset.match(
        this.selectedShapeNode,
        factory.namedNode(prefixes.sh + 'targetClass'),
      )) {
        return targetClass.object;
      }
      for (const targetClass of this.dataset.match(
        this.selectedShapeNode,
        factory.namedNode(prefixes.rdf + 'type'),
        factory.namedNode(prefixes.rdfs + 'Class'),
      )) {
        return targetClass.subject;
      }
      for (const targetClass of this.dataset.match(
        null,
        factory.namedNode(prefixes.sh + 'targetClass'),
      )) {
        return targetClass.object;
      }
      return this.selectedShapeNode;
    },
  },
  watch: {
    fixedValues() {
      this.internalFixedValues = JSON.parse(JSON.stringify(this.fixedValues));
    },
    async formData() {
      await this.parseFormData();
      // Don't replace metadata if cleanedMetadata only contains the same values without the "" ones
      if (
        this.areDatasetsDifferent(
          this.cleanedMetadata,
          this.metadata,
          false,
          true,
        )
      ) {
        this.metadata = this.cleanedMetadata;
      }
      this.initMetadata();
      this.triggerMetadataValidation(
        this.cleanedMetadata,
        this.dataset,
        this.metadataSubject,
        this.validationContext,
        this.ignoreTemplateValues,
      );
    },
    locale() {
      this.setLocale();
    },
    shapes() {
      this.handleApplicationProfiles();
      this.initMetadata();
    },
    targetClass() {
      this.initMetadata();
    },
  },
  async beforeMount() {
    this.setLocale();
    await this.parseFormData();
    this.metadata = this.cleanedMetadata;
    this.internalFixedValues = JSON.parse(JSON.stringify(this.fixedValues));
    this.handleApplicationProfiles();
    this.initMetadata();
  },
  methods: {
    areDatasetsDifferent(
      firstDataset: Dataset,
      secondDataset: Dataset,
      cleanFirst = false,
      cleanSecond = false,
    ) {
      let compareFirstDataset = firstDataset;
      if (cleanFirst) {
        compareFirstDataset = this.createCleanedMetadata(compareFirstDataset);
      }
      let compareSecondDataset = secondDataset;
      if (cleanSecond) {
        compareSecondDataset = this.createCleanedMetadata(compareSecondDataset);
      }

      return (
        !compareFirstDataset.every((entry) =>
          compareSecondDataset.some(
            (newEntry) =>
              newEntry.predicate.value === entry.predicate.value &&
              newEntry.object.value === entry.object.value,
          ),
        ) ||
        !compareSecondDataset.every((entry) =>
          compareFirstDataset.some(
            (newEntry) =>
              newEntry.predicate.value === entry.predicate.value &&
              newEntry.object.value === entry.object.value,
          ),
        )
      );
    },
    compareEntries(entry1: Quad, entry2: Quad) {
      let entry1Order: number = Number.MAX_VALUE;
      let entry2Order: number = Number.MAX_VALUE;
      if (entry1.subject.value in this.orders) {
        entry1Order = this.orders[entry1.subject.value];
      }
      if (entry2.subject.value in this.orders) {
        entry2Order = this.orders[entry2.subject.value];
      }
      if (entry1Order === entry2Order) {
        return 0;
      }
      return entry1Order > entry2Order ? 1 : -1;
    },
    initMetadata() {
      if (this.directlyCreateRdfType || this.cleanedMetadata.size > 0) {
        this.updateMetadata(prefixes.rdf + 'type', [this.targetClass]);
      }
    },
    handleApplicationProfiles() {
      this.triggerMetadataValidation(
        this.cleanedMetadata,
        this.dataset,
        this.metadataSubject,
        this.validationContext,
        this.ignoreTemplateValues,
      );
    },
    updateMetadata(nodeName: string, values: Quad_Object[]) {
      const node = factory.namedNode(nodeName);
      const oldValues = Array.from(
        this.metadata.match(this.metadataSubject, node),
      );
      if (
        oldValues.length !== values.length ||
        !oldValues.every((entry) =>
          values.some((newEntry) => newEntry.value === entry.object.value),
        ) ||
        !values.every((entry) =>
          oldValues.some((newEntry) => newEntry.object.value === entry.value),
        )
      ) {
        this.metadata.deleteMatches(this.metadataSubject, node);
        this.setMetadata(
          values.map((value) =>
            factory.quad(this.metadataSubject, node, value),
          ),
        );
      }
    },
    setLocale() {
      if (this.locale === 'en' || this.locale === 'de') {
        this.localeKey = this.locale;
      } else {
        this.localeKey = 'en';
      }
    },
    setMetadata(values: Dataset | Quad[]) {
      this.metadata.addAll(values);
      // Reactivity Trigger
      this.metadata = (
        this.metadata as unknown as DatasetExt
      ).clone() as unknown as Dataset;
      const newCleanedMetadata = this.createCleanedMetadata(this.metadata);
      if (this.areDatasetsDifferent(newCleanedMetadata, this.cleanedMetadata)) {
        this.cleanedMetadata = newCleanedMetadata;
        this.$emit('update:form-data', this.cleanedMetadata);
        this.triggerMetadataValidation(
          this.cleanedMetadata,
          this.dataset,
          this.metadataSubject,
          this.validationContext,
          this.ignoreTemplateValues,
        );
      }
    },
    triggerUpdateDataset(value: Quad_Object, dataset: Dataset) {
      const collectedNodes = [value];
      const toLookAtNodes = [value];
      const oldValues: Quad[] = [];
      while (toLookAtNodes.length > 0) {
        const currentNode = toLookAtNodes.pop();
        const currentOldValues = Array.from(this.metadata.match(currentNode));
        oldValues.push(...currentOldValues);
        toLookAtNodes.push(
          ...currentOldValues
            .filter(
              (currentOldValue) =>
                (currentOldValue.object.termType === 'NamedNode' ||
                  currentOldValue.object.termType === 'BlankNode') &&
                !collectedNodes.some(
                  (collectedNode) =>
                    collectedNode.value === currentOldValue.object.value,
                ),
            )
            .map((quad) => quad.object),
        );
        collectedNodes.push(...currentOldValues.map((quad) => quad.object));
      }
      const values = Array.from(dataset);
      for (const oldValue of oldValues) {
        this.metadata.delete(oldValue);
      }
      this.setMetadata(values);
    },
    createCleanedMetadata(metadata: Dataset) {
      let cleanedMetadata = metadata.filter(
        (entry) => entry.object.value !== '',
      );
      cleanedMetadata = cleanedMetadata.filter(
        (entry) =>
          entry.object.termType !== 'BlankNode' ||
          cleanedMetadata.some(
            (quad) => quad.subject.value === entry.object.value,
          ),
      );
      cleanedMetadata = cleanedMetadata.filter(
        (entry) =>
          entry.predicate.value !== prefixes.rdf + 'type' ||
          cleanedMetadata.match(entry.subject).size > 1,
      );
      return cleanedMetadata;
    },
    inputFixedValues(nodeName: string, object: FixedValueObject) {
      let different = false;
      if (this.internalFixedValues[nodeName]) {
        const previousObject = this.internalFixedValues[nodeName];
        if (
          previousObject[DefaultFixedValue]?.length ===
            object[DefaultFixedValue]?.length &&
          previousObject[InvisibleFixedValue]?.length ===
            object[InvisibleFixedValue]?.length &&
          previousObject[FixedFixedValue]?.length ===
            object[FixedFixedValue]?.length
        ) {
          // If some property is different between the old and new object
          if (
            !(
              this.checkPropertyExistsInOtherList(
                previousObject[DefaultFixedValue],
                object[DefaultFixedValue],
              ) &&
              this.checkPropertyExistsInOtherList(
                object[DefaultFixedValue],
                previousObject[DefaultFixedValue],
              ) &&
              this.checkPropertyExistsInOtherList(
                previousObject[InvisibleFixedValue],
                object[InvisibleFixedValue],
              ) &&
              this.checkPropertyExistsInOtherList(
                object[InvisibleFixedValue],
                previousObject[InvisibleFixedValue],
              ) &&
              this.checkPropertyExistsInOtherList(
                previousObject[FixedFixedValue],
                object[FixedFixedValue],
              ) &&
              this.checkPropertyExistsInOtherList(
                object[FixedFixedValue],
                previousObject[FixedFixedValue],
              )
            )
          ) {
            different = true;
          }
        } else {
          different = true;
        }
      } else {
        different = true;
      }
      this.internalFixedValues[nodeName] = object;
      if (different) {
        this.$emit(
          'update:fixed-values',
          this.internalFixedValues,
          this.targetClass,
        );
      }
    },
    checkPropertyExistsInOtherList(first?: ValueType[], second?: ValueType[]) {
      return (
        !first ||
        first.every(
          (entry) =>
            second && second.some((newEntry) => entry.value === newEntry.value),
        )
      );
    },
    async parseFormData() {
      let parsedMetadata: Dataset;
      if (typeof this.formData === 'string') {
        parsedMetadata = await this.retrieveDataset(
          this.formData,
          this.formDataMimeType,
          this.selectedShapeValue,
        );
      } else {
        parsedMetadata = factory.dataset(
          Array.from(this.formData),
        ) as unknown as Dataset;
      }
      if (this.areDatasetsDifferent(parsedMetadata, this.cleanedMetadata)) {
        this.cleanedMetadata = parsedMetadata;
      }
    },
  },
});
</script>

<style>
#FormGenerator .invalid-tooltip {
  position: absolute;
  top: 100%;
  z-index: 5;
  display: none;
  max-width: 100%;
  padding: 0.25rem 0.5rem;
  margin-top: 0.1rem;
  font-size: 0.76563rem;
  line-height: 1.4;
  color: #fff;
  background-color: rgba(204, 7, 30, 0.9);
  border-radius: 0;
}
#FormGenerator .btn-float-right {
  float: right;
}
#FormGenerator .col-form-label.mandatory:after {
  content: ' *';
  color: #cc071e;
}
[data-bs-theme='dark'] #FormGenerator .col-form-label.mandatory:after {
  color: #e06a78;
}
#FormGenerator .innerButton {
  height: calc(1.5em + 0.75rem + 2px);
  margin: 0px !important;
  min-width: 60px !important;
  width: 60px !important;
  max-width: 60px !important;
}
#FormGenerator .innerButton svg {
  margin: auto;
}
#FormGenerator .metadata .material-design-icon__svg {
  margin-top: -0.1em;
}
#FormGenerator .lock-colum {
  padding-left: 0;
  margin-top: 0px;
}
#FormGenerator .form-group.invisible {
  display: none;
}
</style>
