import prefixes from '@zazuko/prefixes/prefixes';
import factory from 'rdf-ext';
import type { TerminologyResponse } from '@/types/terminologyResponse';
import type { BilingualLabel, BilingualLabels } from '@/types/labels';
import type {
  Dataset,
  Literal,
  Quad,
  Quad_Object,
  Quad_Predicate,
  Quad_Subject,
} from '@rdfjs/types';
import { rdfParser } from 'rdf-parse';
import { Readable } from 'stream';
import type QuadExt from 'rdf-ext/lib/Quad';

export function getObject(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
  locale?: string,
): Quad[] {
  let results = Array.from(dataset.match(subject, predicate));
  if (locale !== undefined) {
    results = results.sort((quad) =>
      (quad.object as Literal).language === locale ? -1 : 1,
    );
    if (results.length > 0) {
      return [results[0]];
    }
  }
  return results;
}

export function getObjectQuadList(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
): Quad[] {
  let results = Array.from(dataset.match(subject, predicate));
  if (results.length > 0) {
    // Assume it is an rdf list and go through with it
    const quadList: Quad[] = [];
    let currentSubject = results[0].object;
    const rdfFirstNode = factory.namedNode(prefixes.rdf + 'first');
    const rdfRestNode = factory.namedNode(prefixes.rdf + 'rest');
    const rdfNilNode = factory.namedNode(prefixes.rdf + 'nil');
    if (!currentSubject.equals(rdfNilNode)) {
      quadList.push(results[0]);
      do {
        results = Array.from(dataset.match(currentSubject, rdfFirstNode));
        if (results.length > 0) {
          quadList.push(results[0]);
        }
        results = Array.from(dataset.match(currentSubject, rdfRestNode));
        if (results.length > 0) {
          quadList.push(results[0]);
          currentSubject = results[0].object;
        } else {
          break;
        }
      } while (!currentSubject.equals(rdfNilNode));
    }
    return quadList;
  }
  return [];
}

export async function getQuads(
  data: string,
  mimeType: string,
  baseUri: string | undefined,
): Promise<Dataset> {
  const input = new Readable({
    read: () => {
      input.push(data);
      input.push(null);
    },
  });
  const dataset = factory.dataset();
  await new Promise((resolve) => {
    rdfParser
      .parse(input, { contentType: mimeType, baseIRI: baseUri })
      .on('data', (quad: QuadExt) => {
        dataset.add(quad);
      })
      .on('end', () => resolve(dataset));
  });
  return dataset as unknown as Dataset;
}

export function getObjectList(
  subject: Quad_Subject,
  predicate: Quad_Predicate,
  dataset: Dataset,
): Quad_Object[] {
  const rdfFirstNode = factory.namedNode(prefixes.rdf + 'first');
  return getObjectQuadList(subject, predicate, dataset)
    .filter((quad) => quad.predicate.equals(rdfFirstNode))
    .map((quad) => quad.object);
}

export async function retrieveChildren(
  classUrl: string,
  pageNumber: number,
  pageSize: number,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  searchTerm?: string,
): Promise<BilingualLabels> {
  const response = await fetch(
    `https://service.tib.eu/ts4tib/api/search?q=*&obsoletes=false&local=false&groupField=iri&childrenOf=${classUrl}&start=${pageNumber}&rows=${pageSize}&format=json`,
  );
  const tibResponse: TerminologyResponse = await response.json();
  const de =
    tibResponse.response?.docs.map((doc) => {
      return {
        name: doc.label,
        value: doc.iri,
      };
    }) ?? [];
  return {
    de,
    en: de,
  };
}

export async function retrieveInstance(
  instanceUrl: string,
  classUrl?: string,
): Promise<BilingualLabel> {
  const response = await fetch(
    `https://service.tib.eu/ts4tib/api/search?q=${instanceUrl}&obsoletes=false&local=false&groupField=iri&childrenOf=${classUrl}&format=json`,
  );
  const tibResponse: TerminologyResponse = await response.json();
  const de =
    tibResponse.response?.docs.map((doc) => {
      return {
        name: doc.label,
        value: doc.iri,
      };
    }) ?? [];
  return {
    de: de[0],
    en: de[0],
  };
}
