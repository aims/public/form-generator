import VueMultiselect from 'vue-multiselect';

import 'vue-multiselect/dist/vue-multiselect.css';
import './_custom.css';

export default VueMultiselect;
